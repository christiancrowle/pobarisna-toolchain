SCRIPT=`realpath $0`
SCRIPTPATH=`dirname $SCRIPT`
export PREFIX="$SCRIPTPATH/cross-prefix"
export TARGET=powerpc-elf
export PATH="$PREFIX/bin:$PATH"
export TOOLCHAINENVSOURCED="yep"
export JOBS="10"

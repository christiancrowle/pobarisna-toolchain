#!/bin/bash

LIGHT_PURP='\033[1;35m'
LIGHT_GREEN='\033[1;32m'
RED='\033[1;31m'
CYAN='\033[1;36m'
NO_COLOUR="\033[0m"

# if we're on macos, we'll need to use the homebrew gmp/mpfr when configuring gcc
if [[ "$OSTYPE" == "darwin"* ]]; then
    export MACOS="-macos"
fi

# silent pushd/popd helpers
pushd() {
    command pushd "$@" > /dev/null
}

popd() {
    command popd "$@" > /dev/null
}

echo -e "${CYAN}Building a ${TARGET} toolchain...${NO_COLOUR}"
if [ -z ${TOOLCHAINENVSOURCED+x} ]; then 
    echo -e "${RED}ERROR: you didn't modify/source env.sh >:(${NO_COLOUR}"
    exit -1
fi

pushd build
    pushd binutils
        echo -e "  ${LIGHT_PURP}Configuring binutils!${NO_COLOUR}"
        ./config-binutils.sh &> binutils-config.log
        if [ $? -ne 0 ]; then
            echo -e "    ${RED}Failed! See `pwd`/binutils-config.log${NO_COLOUR}"
            popd ; popd
            exit $?
        fi
        echo -e "  ${LIGHT_PURP}Building binutils!${NO_COLOUR}"
        ./build-binutils.sh &> binutils-build.log
        if [ $? -ne 0 ]; then
            echo -e "    ${RED}Failed! See `pwd`/binutils-build.log${NO_COLOUR}"
            popd ; popd
            exit $?
        fi 
    popd

    pushd gcc_stage1
        echo -e "  ${LIGHT_PURP}Configuring gcc stage 1!${NO_COLOUR}"
        ./config-gcc${MACOS}.sh &> gcc-stage1-config.log
        if [ $? -ne 0 ]; then
            echo -e "    ${RED}Failed! See `pwd`/gcc-stage1-config.log${NO_COLOUR}"
            popd ; popd
            exit $?
        fi
        echo -e "  ${LIGHT_PURP}Building gcc stage 1!${NO_COLOUR}"
        ./build-gcc.sh &> gcc-stage1-build.log
        if [ $? -ne 0 ]; then
            echo -e "    ${RED}Failed! See `pwd`/gcc-stage1-build.log${NO_COLOUR}"
            popd ; popd
            exit $?
        fi
    popd

    pushd newlib
        echo -e "  ${LIGHT_PURP}Configuring newlib!${NO_COLOUR}"
        ./config-newlib.sh &> newlib-config.log
        if [ $? -ne 0 ]; then
            echo -e "    ${RED}Failed! See `pwd`/newlib-config.log${NO_COLOUR}"
            popd ; popd
            exit $?
        fi
        echo -e "  ${LIGHT_PURP}Building newlib!${NO_COLOUR}"
        ./build-newlib.sh &> newlib-build.log
        if [ $? -ne 0 ]; then
            echo -e "    ${RED}Failed! See `pwd`/newlib-build.log${NO_COLOUR}"
            popd ; popd
            exit $?
        fi
    popd

    pushd gcc
        echo -e "  ${LIGHT_PURP}Configuring gcc for real!${NO_COLOUR}"
        ./config-gcc${MACOS}.sh &> gcc-config.log
        if [ $? -ne 0 ]; then
            echo -e "    ${RED}Failed! See `pwd`/gcc-config.log"
            popd ; popd
            exit $?
        fi
        echo -e "  ${LIGHT_PURP}Building gcc for real!${NO_COLOUR}"
        ./build-gcc.sh &> gcc-build.log
        if [ $? -ne 0 ]; then
            echo -e "    ${RED}Failed! See `pwd`/gcc-build.log"
            popd ; popd
            exit $?
        fi
    popd
popd

echo -e "${LIGHT_GREEN}Congrats! You should now have a working toolchain in ${CYAN}${PREFIX}${LIGHT_GREEN}. Source env.sh before using!${NO_COLOUR}"

# POBARISNA toolchain scripts

Decided I should put these here before I lose them.

To build a working toolchain, all you *should* need to do is the following. (assuming you already have some sort of GCC or clang)

1. Download tarballs for newlib, gcc, and binutils, put them in this folder, and extract them.

1. Install gmp and mpfr. These should be in your package manager. On macOS, you'll need to install them through `brew` unless you modify the scripts.

2. Modify env.sh to set PREFIX and JOBS to your liking.

3. `source env.sh`

4. Run build.sh

You can build other types of cross toolchains with a similar config, too. 
Just change TARGET to something appropriate in env.sh.

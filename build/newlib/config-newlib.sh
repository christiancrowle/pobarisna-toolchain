#!/bin/sh

../../newlib-*/configure --prefix=$PREFIX --target=$TARGET --disable-newlib-supplied-syscalls CFLAGS="-DMALLOC_PROVIDED" CXXFLAGS="-DMALLOC_PROVIDED" \
    host_configargs="CFLAGS=\"-DMALLOC_PROVIDED\" CXXFLAGS=\"-DMALLOC_PROVIDED\"" target_configargs="CFLAGS=\"-DMALLOC_PROVIDED\" CXXFLAGS=\"-DMALLOC_PROVIDED\""
exit $?
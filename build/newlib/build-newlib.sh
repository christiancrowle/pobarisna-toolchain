#!/bin/sh

make -j${JOBS}
if [ $? -ne 0 ]; then
    exit $?
fi
make install
if [ $? -ne 0 ]; then
    exit $?
fi
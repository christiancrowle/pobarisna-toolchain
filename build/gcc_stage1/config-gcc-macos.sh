#!/bin/sh

../../gcc-*/configure --prefix=$PREFIX --target=$TARGET --enable-lto --enable-languages=c,c++ --without-headers \
    --with-newlib --enable-interwork --enable-multilib --disable-shared --with-gmp=/opt/homebrew --with-mpfr=/opt/homebrew
exit $?
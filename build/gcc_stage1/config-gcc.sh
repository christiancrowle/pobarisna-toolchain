#!/bin/sh

../../gcc-*/configure --prefix=$PREFIX --target=$TARGET --enable-lto --enable-languages=c --without-headers \
    --with-newlib --enable-interwork --enable-multilib --disable-shared
exit $?
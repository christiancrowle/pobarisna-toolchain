#!/bin/sh

make all-gcc -j${JOBS}
if [ $? -ne 0 ]; then
    exit $?
fi
make all-target-libgcc -j${JOBS}
if [ $? -ne 0 ]; then
    exit $?
fi
make install-gcc
if [ $? -ne 0 ]; then
    exit $?
fi
make install-target-libgcc
if [ $? -ne 0 ]; then
    exit $?
fi
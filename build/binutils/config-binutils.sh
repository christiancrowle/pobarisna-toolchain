#!/bin/sh

../../binutils-*/configure --prefix=$PREFIX --target=$TARGET --enable-lto --enable-interwork --enable-multilib
exit $?